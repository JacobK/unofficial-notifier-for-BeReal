# SPDX-FileCopyrightText: 2022, 2023 Jacob K
#
# SPDX-License-Identifier: GPL-3.0-or-later

# This script waits for today's BeReal and then calls bedaily.sh.

if [[ ! -z $(./checkvars.sh) ]]; then exit; fi

timezone=$bedailynotifier_timezone
api_endpoint=$bedailynotifier_api_endpoint # where to get the json times from

bereal_happened=0 # keeps track of whether or not the BeReal has happened today

# The official app sends a server request like once per second, so sending the request very often should be okay.
while [[ $bereal_happened == 0 ]]
do
	echo "Sending server request at local time of `date`"
#	notify-send "$app_name" "debug: sending server request at $current_time" # debug, to make sure we don't do this too often
	export bedailynotifier_bereal_json=`curl $api_endpoint`
	bereal_json=$bedailynotifier_bereal_json
	if [[ -z "$bereal_json" ]]
	then # Probably internet broke
		echo "Recieved no response from $api_endpoint"
		echo "Maybe you are disconnected from the internet?"
	else
		current_time=`echo $bereal_json | jshon -e "now" -e "ts"` # For some reason, the current time is an integer instead of a string like the region times.
		day_of_week=`date --date="@$current_time" +%u` # current day of the week, used as a check to make sure that we don't see that the current time is after yesterday's BeReal and conclude that "the" BeReal already happened
		bereal_time=`echo $bereal_json | jshon -e "regions" -e "$timezone" -e "ts" | tr -d '"'`
			# Thanks to https://stackoverflow.com/a/26314887 for the `tr -d '"'` code.
		bereal_day_of_week=`date --date="@$bereal_time" +%u` # compare to day_of_week for above check
		if [[ $day_of_week == $bereal_day_of_week ]]
		then
			if [[ $current_time -gt $bereal_time ]] # If current time is > BeReal time (-gt means greater than)
			then
				bereal_happened=1
				echo "BeReal happened at `date --date="@$bereal_time"`!"
			fi
		fi
	fi
	
	# If the BeReal didn't happen, sleep for 15 seconds before repeating the loop.
	if [[ $bereal_happened == 0 ]]
	then
		sleep 15
	fi
done

./bedaily.sh # Take the BeReal (You'll have to plug in USB camera and switch to that before exiting Cheese.)
