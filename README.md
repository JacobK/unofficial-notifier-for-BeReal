# BeDaily - Unofficial Notifier for BeReal

## Getting an API key

The third party API service used by this program has updated to require an API key, and each user must request one before using this program. Put your API key in a file called apikey.txt. To get your API key, go to https://bereal.devin.fun and run the script `document.getElementById("dashboard").classList.remove("show");document.getElementById("dashboard").classList.remove("active");document.getElementById("api").classList.add("show");document.getElementById("api").classList.add("active");` and then click on "Request API Key". This will take you to a Google Form, which you will need to use Haketilo to fill out. I didn't actually have to fill out this form myself, so if you try it, let me know if you are able to get an API key that way!

## Program Instructions

This program is somewhat unintuitive to use currently. First, if you are in a timezone that is not us-central, head over to main.sh and edit the bedailynotifier\_timezone variable to your timezone. Then, to start the program, simply run main.sh. When the BeReal does happen, a screenshot will be taken automatically and saved in $XDG\_DATA\_HOME/bedaily-notifier/pictures/YYYY-MM-DD and Cheese will open. While Cheese is open, you should attach any external webcam(s) you want to use, and you can test them in Cheese to make sure they are connected properly (You can also take pictures in Cheese if you want, but they will be saved to Cheese's folders, not to the data directory.). When you are ready, close Cheese by pressing the x button in the top right, and pictures from webcams 0-4 will be taken and saved to the YYYY-MM-DD folder as well. If a webcam with a particular ID does not exist, the 0 byte output will be automatically deleted. Finally, Caja will be opened at the location of the pictures so you can see and share the pictures taken. The program will then wait for tomorrow and start againak fi, so you don't need to start the program again unless you restart your computer or close it with ctrl + C or something like that.

This program was developed and tested on Trisquel. If you use an operating system with a different camera app or file manager, you'll probably want to modify bedaily.sh accordingly. The required dependencies as it is now are `curl jshon kde-spectacle libnotify-bin cheese vgrabbj caja`, and can be installed with `sudo apt install curl jshon kde-spectacle libnotify-bin cheese vgrabbj caja` if not already installed.

This program uses a third party API service, not made by BeReal or me, hosted at https://bereal.devin.fun

## Known (possible) Issues

* The program must be run from the folder containing it.

* If the time comes to take the picture, but you fail to do so before midnight, I think the program will wait until the day after you took the picture, leading to a skipped BeDaily.

* If your system timezone does not match the timezone set, the boundary between days might be improperly defined, which may lead to some problems, including maybe making the deadline to take the pictures without skipping a BeDaily different. More work is needed to ignore system timezones and only consider the BeReal timezone.

* If you have a camera device that isn't at /dev/video? where '?' is a number 0 through 4, then no picture will be taken using that camera.
