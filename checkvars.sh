# SPDX-FileCopyrightText: 2023 Jacob K
#
# SPDX-License-Identifier: GPL-3.0-or-later

if [[ -z "$bedailynotifier_data" || -z "$bedailynotifier_api_endpoint" || -z "$bedailynotifier_timezone" ]]
then
echo "bad"
echo "Hey! You need to run this script from main.sh, or manually set the variables checked in checkvars.sh." 1>&2
exit
fi
