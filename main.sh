# SPDX-FileCopyrightText: 2023 Jacob K
#
# SPDX-License-Identifier: GPL-3.0-or-later

# This is the main entry point for BeDaily. It defines the bedailynotifier_data directory and makes sure that it exists, and then it enters the loop.

# constants
export bedailynotifier_app_name="BeDaily - Notifier for BeReal" # display name in notifications
bedailynotifier_api_url="https://bereal.devin.rest/v1/moments/latest"
bedailynotifier_api_attribution_message="The BeReal time information comes from BeReal Time History API By Devin Baeten."
bedailynotifier_api_key=`cat apikey.txt`
export bedailynotifier_api_endpoint="$bedailynotifier_api_url?api_key=$bedailynotifier_api_key" # where to get the json times from
export bedailynotifier_timezone="us-central" # BeReal time zone, can be "us-central", "europe-west", "asia-west", or "asia-east"

echo $bedailynotifier_api_attribution_message

# If HOME isn't set, exit because we don't know what to do in that case.
if [[ -z "$HOME" ]]
then
	echo "HOME is not set. I don't know where to put your pictures in that case. Exiting."
	exit
fi

# If XDG_DATA_HOME is not set, use the default of $HOME/.local/share. Otherwise, use XDG_DATA_HOME.
if [[ -z "$XDG_DATA_HOME" ]]
then
	export bedailynotifier_data=$HOME/.local/share/bedaily-notifier
else
	export bedailynotifier_data=$XDG_DATA_HOME/bedaily-notifier
fi

pictures=$bedailynotifier_data/pictures # temporary variable, used to create the directory

echo "Pictures will be stored in $bedailynotifier_data/pictures"

# If the bedailynotifier_data directory does not exist, create it.
if [[ ! -d "$bedailynotifier_data" ]]
then
	echo "Creating save data folder at $bedailynotifier_data"
	mkdir $bedailynotifier_data
fi

# If the pictures directory does not exist, create it.
if [[ ! -d "$pictures" ]]
then
	echo "Creating pictures folder at $pictures"
	mkdir $pictures
fi

# Enter the main loop, starting with today's BeReal.
./infiniteloop.sh
