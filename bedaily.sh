# SPDX-FileCopyrightText: 2022, 2023 Jacob K
#
# SPDX-License-Identifier: GPL-3.0-or-later

# This script creates a folder corresponding to today, and then takes a screenshot as well as pictures with the camera, and then it opens caja so you can see and share your pictures.
# If today's folder already exists, the script will exit.

if [[ ! -z $(./checkvars.sh) ]]; then exit; fi

if [[ -z "$bedailynotifier_bereal_json" ]]
then
echo "Missing response from server. Exiting"
exit
fi

# set app name just in case
if [[ -z "$bedailynotifier_app_name" ]]
then
app_name="null"
else
app_name=$bedailynotifier_app_name
fi

pictures=$bedailynotifier_data/pictures

cd $pictures
date=`date --date="@\`echo $bedailynotifier_bereal_json | jshon -e "now" -e "ts"\`" +%Y-%m-%d` # Get a YYYY-MM-DD date string based on the server JSON

# Check if the current date already exists, and exit if so.
if [[ -d "$date" ]]
then
echo "You've already taken today's picture! No pictures were taken."
notify-send "$app_name" "You've already taken today's picture! No pictures were taken."
exit
fi

mkdir $date
cd $date

spectacle -b -o $date\_screenshot.png & # (& means in the background) take a screenshot first, could add -n to avoid the notification, but I think it's fine
	# TODO: make spectacle a child process or something because it seems to get stuck sometimes

notify-send "$app_name" "The BeDaily is now, at `date --date="@$bereal_time"`!"

# TODO: put each instance of vgrabbj in it's own thread, using fork() in C or something similar
echo "Close cheese to take the pictures!"
cheese # Close cheese to take the pictures
for i in 0 1 2 3 4 # TODO: enumerate all the video devices rather than hardcoding (can say `for DEVICE in /dev/video*` but then not sure how to name the file from there)
do
	# TODO: find a way to get cheese-quality photos automatically (vgrabbj with default settings results in a very dark photograph for me, on my internal webcam)
	vgrabbj -d/dev/video$i -isvga -z2 > $date\_$i.jpg
done

find $date* -size 0 -delete # delete empty files (e.g. from cameras that don't really exist)
	# Thanks to https://stackoverflow.com/a/5475933 for this code

caja . # Open Caja so you can see and share the photos
