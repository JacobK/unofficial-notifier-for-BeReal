# SPDX-FileCopyrightText: 2023 Jacob K
#
# SPDX-License-Identifier: GPL-3.0-or-later

# This script waits for tomorrow.

if [[ ! -z $(./checkvars.sh) ]]; then exit; fi

api_endpoint=$bedailynotifier_api_endpoint

echo "Waiting for the next day..."

# TODO: Move this loop to infiniteloop so that if one takes the pictures after midnight it doesn't wait until the day after they took the pictures
got_start_time="false"
bereal_json=""
while [[ "$got_start_time" == "false" ]]
do
	sleep 30
	bereal_json=`curl $api_endpoint`
	if [[ -z "$bereal_json" ]]
	then # Probably internet broke
		echo "Recieved no response from $api_endpoint"
		echo "Maybe you are disconnected from the internet?"
	else
		got_start_time="true"
	fi
done

start_day_of_week=`date --date="@\`echo $bereal_json | jshon -e "now" -e "ts"\`" +%u`
current_day_of_week=`date --date="@\`echo $bereal_json | jshon -e "now" -e "ts"\`" +%u`

while [[ "$start_day_of_week" == "$current_day_of_week" ]]
do
	echo "Waiting for the next day..."
	sleep 300 # sleep for 5 minutes
	bereal_json=`curl $api_endpoint`
	if [[ -z "$bereal_json" ]]
	then # Probably internet broke
		echo "Recieved no response from $api_endpoint"
		echo "Maybe you are disconnected from the internet?"
	else
		current_day_of_week=`date --date="@\`echo $bereal_json | jshon -e "now" -e "ts"\`" +%u`
	fi
done
