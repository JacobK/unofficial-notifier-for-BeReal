# SPDX-FileCopyrightText: 2023 Jacob K
#
# SPDX-License-Identifier: GPL-3.0-or-later

# This script alternates between running notify.sh, which waits for the BeReal and then runs bedaily.sh, and nextday.sh, which waits for the next day.

if [[ ! -z $(./checkvars.sh) ]]; then exit; fi

while [[ true ]]
do
./notify.sh
./nextday.sh
done
